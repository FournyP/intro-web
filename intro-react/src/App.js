import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import './App.css';

function App() {
  
  const [isClicked, setIsClicked] = useState(false);
  // const [items, setItems] = useState();

  useEffect(() => {

    console.log(isClicked);

    if (isClicked) {

      var xhr = new XMLHttpRequest();
      xhr.addEventListener('load', () => {
        
        let json = JSON.parse(xhr.responseText);

        console.log(json);

        let elements = []
        
        json.features.forEach(feature => {
          elements.push(
            React.createElement("div", {},
              React.createElement("h4", {}, feature.properties.nom),
              React.createElement("ul", {},
                React.createElement("p", {}, "Telephone : " + feature.properties.telephone),
                React.createElement("p", {}, "Code postal : " + feature.properties.codeInsee),
                React.createElement("p", {}, "URL : " + feature.properties.url)
            )
          ));
        });

        ReactDOM.render(
          elements,
          document.getElementById('result-area')
        );
      });

      let selectedDepartement = document.getElementById("select-department").value;
      let selectedAdministration = document.getElementById("select-administration-type").value;

      xhr.open('GET', 'https://etablissements-publics.api.gouv.fr/v3/departements/' + selectedDepartement + '/' + selectedAdministration);
      // send the request
      xhr.send();

    }
    setIsClicked(false)
  }, [isClicked]);


  return (
    <div className="App">
      <h1>Trouver son administration en Normandie</h1>

      <select id="select-department">
        <option value="14">Calvados</option>
        <option value="27">Eure</option>
        <option value="50">Manche</option>
        <option value="61">Orne</option>
        <option value="76">Seine Maritine</option>
      </select>
      <select id="select-administration-type">
        <option value="ars">Agence Regionale de Santé</option>
        <option value="cpam">Caisse Primaire d'Assurance Maladie</option>
      </select>
      <button name="search-administration" onClick={() => setIsClicked(true)}>
        Rechercher une administration</button>
      <button name="clear-research">Vider la recherche</button>

      <div id="result-area">
        
      </div>
    </div>

  );
}

export default App;
