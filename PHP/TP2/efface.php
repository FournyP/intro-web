<?php

    session_start();

    if(!isset($_SESSION["connecte"]) && $_SESSION["connecte"] !== true)
    {
        header("Location: ./login.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <a href="./deconnexion.php"><button>Deconnexion</button></a>
        <div>
            <p>Bienvenue <?= $_SESSION["identidiant"]?></p>
        </div>
    </body>
</html>